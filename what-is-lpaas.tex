%=======================================================================
\chapter{What is \lpaas{}}
\label{what-is}
%=======================================================================

\lpaas{} is an open-source, light-weight Prolog framework for distributed applications and infrastructures, released under the LGPL license, available from \url{http://lpaas.apice.unibo.it}.

We propose \lpaas{} \cite{lpaas-tplp18} as the reference architecture for delivering micro-intelligence in distributed systems (i.e. such as MAS) in compliance with the situated LP conceptual framework (SLP) \cite{lpaas-aamas19}.
\lpaas{} is  a logic-based, service-oriented architecture conceived and designed as the natural evolution of LP in nowadays pervasive computing systems---thus, as the natural instantiation of the SLP idea.
%
The main goal of \lpaas{} is to enable situated reasoning via explicit definition of the spatio-temporal structure of the environment where situated entities act and interact, re-interpreting the notion of \emph{distribution} of LP accordingly to the SLP framework.

The \lpaas{} architectural style and design principles adhere to the Service-Oriented Architecture (SOA) paradigm -- and, in particular, adopt the microservices vision \cite{lpaas-ijguc2018} -- since SOA is the \emph{de facto} standard for distributed application development \cite{soabook-2005}.
%
This choice further emphasises the role of \emph{situatedness}, already brought along by distribution in itself, that is, developing the idea of LP as a \emph{situated service} while promoting key features such as encapsulation, statelessness, and locality.


%===============================================================================
\section{The \lpaas{} Vision}
\labelsec{vision}
%===============================================================================

The evolution of LP in parallel, concurrent, and distributed scenarios is the main motivation for re-interpreting the notion of \emph{distribution} of LP in today's context.
%
Since Service-Oriented Architecture (\soa{}) is the \emph{de facto} standard for distributed application development in both the academia and the industry \cite{soabook-2005}, \xss{service} focuses on how LP can be re-interpreted in the \emph{service} perspective.
%
This perspective further emphasises the role of \emph{situatedness}, already brought along by distribution in itself: thus, \xss{situatedness} discusses how being situated in space, time, and context affects LP computation.
%
The two novel perspectives are merged together in \xss{towards-situated-service}, which develops the idea of LP as a \emph{situated service}.

%------------------------------------------------------------------------------
\subsection{The Service Perspective}
\labelssec{service}
%------------------------------------------------------------------------------

The service-oriented perspective deeply affects the way in which LP engines are conceived, designed, and used---in particular, as far as the very nature of LP \emph{encapsulation} is concerned, the way in which clients interact (requiring \emph{statelessness}), and the assumptions about the surrounding context (\emph{locality}) are concerned.

%---------------------------------
\paragraph{Encapsulation.}
%---------------------------------
A service hides both data representation and the computational mechanisms behind a public interface exposed to its clients.
%
In the context of LP engines, this means that both the logic theory (the data) and the resolution process (the computational mechanism) are \emph{inaccessible} -- and, in general, \emph{not observable} -- from outside the boundary of the service interface.
%
As a consequence, theory manipulation mechanisms, such as \texttt{assert}/\texttt{retract}, are no longer directly applicable from the client perspective: since the logic theory is encapsulated by the service, dedicated mechanisms are required for its handling.
%
For instance, in an \iot{} scenario, this may happen via a separate ``sensor API'' through which sensor devices regularly update the KB of the LP service according to their perception of the surrounding environment.

Accordingly, the logic theory of a \lpaas{} service can be either \emph{static} or \emph{dynamic} (which are mutually exclusive configurations).
%
The way in which the LP service can be accessed obviously depends on that: time is an issue for dynamic KB, not for static ones.


%---------------------------------
\paragraph{Statelessness.}
%---------------------------------
Encapsulation makes it irrelevant \emph{how} the encapsulated behaviour is implemented: what actually matters are the inputs triggering, and the outputs resulting from, that behaviour.
%
Furthermore, in the \soa{} perspective, services are usually redundantly distributed over a network of hosts for enhancing the service availability and reliability: thus, it does not really matter \emph{who} actually carries out the encapsulated behaviour.
%
In the context of LP, this means that interactions with clients should be also allowed to be \emph{stateless}---that is, include all the information required by the resolution process, since a different component may serve a different request.
%
Notably, statelessness is the default for RESTful web services, too.

It is worth emphasising here that statelessness does not contrast with the above encapsulation property, since the former regards the \emph{invocation} of \lpaas{} services -- hence the interaction between clients and servers --, whereas the latter concerns \lpaas{} services themselves---that is, their inner nature.
%
In other words, statelessness implies that servers are not supposed to track the state of interactions, so that a service request should not assume or rely on previous interactions, whereas encapsulation means that only the selected properties of the service are visible and modifiable from the outside.

At the same time, in order to cope with data-intensive applications, where stateless interaction may become cumbersome, \lpaas{} also supports \emph{stateful} interaction---yet, at the clients' convenience and will.
%
This is particularly handy for scenarios where reasoning and inference should be based on continuous and possibly unbounded streams of data, such as those coming from sensors in \iot{} deployments.

%---------------------------------
\paragraph{Locality.}
%---------------------------------
The distributed nature of the system drastically changes the perspective over consistency:
maintaining \emph{globally-consistent} information is typically unfeasible in such systems.
%
Furthermore, when pervasive systems enter the picture, even \emph{globally-available} information is usually not a realistic assumption: for instance, in \iot{} scenarios, heterogeneous data streams are continuously made available by sensor devices scattered in specific portions of the physical environment.
%
As a consequence, encapsulation is inevitably bound to a specific, (\emph{local}) portion of the system---with a notion of locality extending up to when/where availability and/or consistency are inevitably lost.

In the context of LP, this means first of all resorting to a \emph{multi-theory} logical framework, exploiting the typical approach to \emph{modularity} adopted in traditional LP in order to allow for parallel and concurrent computation \cite{bugliesi-jlp19}.
%
Also, locality implies that each logic theory describes just what is \emph{locally} true ---which basically means leaving aside in principle the global acceptation of the \emph{closed world} assumption \cite{closedworld-logicdbbook1978} in favor of a more realistic \emph{locally-closed world} assumption.
%
Accordingly, every LP service is to be queried about \emph{what is locally known to be true}, with no need to resort to global knowledge of any sort---and with no need to distribute the resolution process in any way.


%------------------------------------------------------------------------------
\subsection{The Situatedness Perspective}
\labelssec{situatedness}
%------------------------------------------------------------------------------

The distribution of LP service instances directly calls for \emph{situatedness}, intended as the property of the LP service to be immersed in the surrounding computational/physical environment, whose changes may affect its computations \cite{coordarch-eaai41}.
%
As an example, new sensor data may change the replies of an LP service to queries.
%
Situatedness adds three new dimensions to LP computations: \emph{space}, \emph{time}, and \emph{context}.

%---------------------------------
\paragraph{Space.}
%---------------------------------
To be situated in space means that the \emph{spatial context} where the LP service is executing may affect its properties, computations, and the way it interacts with clients.

Distribution \emph{per se} constitutes a premise to spatial situatedness: each LP instance runs on a different device, thus on a different network host, therefore accessing the different computational and network resources that are \emph{locally} available.
%
Moreover, since LP services encapsulate the logic theory for their resolution process, the locally-gathered knowledge affects the result, once it is represented in terms of logic axions.
		
Also, more articulated forms of spatial situatedness may be envisioned: for instance, \emph{mobile} clients may request LP services from different locations at each request, possibly even \emph{while} moving, which means that the LP service must be able to coherently identify and track clients so as to reply to the correct network address.
%
Finally, it is possible in principle to conceive logic theories -- or even individual axioms therein -- with spatially-bound validity, that is, that are true only in specific points or regions in space---analogously to \emph{spatial tuples} in \cite{spatialtuples-idc2016}.

%---------------------------------
\paragraph{Time.}
%---------------------------------
Complementarily, being situated in time means that the \emph{temporal context} when the LP service is executed may affect its properties, computations, and interactions with clients.
%
Yet again, distribution alone already brings about temporal issues: moving information in a network takes time, thus aspects such as expiration of requests, obsolescence of logic theories, and timeliness of replies should be taken into account when designing the LP service.

Furthermore, since reconstructing a \emph{global} notion of time in pervasive systems is either unfeasible or non-trivial, each LP service should operate on its own local time---that is, computing deadlines, leasing times, and the like according to its \emph{local perception} of time.
%
Also, in the same way as for spatial situatedness, temporal situatedness may also imply that logic theories or individual axioms may have their time-bounded validity---e.g., holding true up to a certain instant in time, and no longer since then.

%---------------------------------
\paragraph{Context.}
%---------------------------------
Besides the space/time fabric, situatedness also regards the generic \emph{environment} within which LP services execute---that is, the computational and physical context which may affect their working cycle: for instance, it may depend on the available CPUs and RAM, whether an accelerometer is available on the current hosting device, etc.
		
A basic level of \emph{contextual situatedness} is already embedded in the very nature of the LP service: in fact, locality of the resolution process implies that the logic theory for goal resolution belongs to the context of the LP service, thus straightforwardly affecting its behaviour.
%
However, especially in the \iot{} scenarios envisioned for \lpaas{}, the computational and physical contexts may both impact the LP service: e.g., sensor devices may continuously update the service KB with their latest perceptions, while actuators may promptly provide feedback on success/failure of physical actions.


%------------------------------------------------------------------------------
\subsection{Towards LP as a Situated Service}
\labelssec{towards-situated-service}
%------------------------------------------------------------------------------

The above perspectives promote a radical re-interpretation of a few facets of LP, moving LP itself towards the notion of \lpaas{} envisioned in this paper---that is, in terms of a \emph{situated service}.
%
Such a notion articulates along four major lines:
\begin{itemize}
  \item the preservation (with re-contextualisation) of the SLD resolution process;
  \item stateless interactions;
  \item time-sensitive computations;
  \item space-sensitive computations.
\end{itemize}

%---------------------------------
\paragraph{The re-contextualisation of the SLD resolution process.}
%---------------------------------
The SLD resolution process \cite{resolution-jacm12} remains a staple in \lpaas{}: yet, it is re-contextualised in the situated nature of the specific LP service.
%
This means that, given the precise \emph{spatial}, \emph{temporal}, and \emph{general} contexts within which the service is operating \emph{when the resolution process starts}, the  process follows the usual rules of SLD resolution: situatedness is accounted for through the service abstraction with respect to such three contexts.

With respect to the \emph{spatial context}, the resolution process obviously takes place in the hosting device \emph{where} the LP service is running, thus taking into account the specific properties of the computational and physical environment therein available -- CPU, RAM, network facilities, GPS positioning, etc.\ -- there included the specific logic theory the LP service relies on.
%
As mentioned in \xss{situatedness}, more articulated forms of spatial situatedness -- e.g., involving mobility of clients (and LP services, in principle), or, virtual/physical regions of validity for logic axioms -- could be envisioned.

The \emph{temporal context} refers to the resolution process taking place on a \emph{frozen snapshot} of the LP service state -- there including its KB --, which stays unaffected to external stimuli (possibly affecting the resolution process) until the process itself terminates.
%
This way, despite the dynamic nature of the KB -- encapsulated by the service abstraction -- which could change e.g. due to sensors' perceptions, the resolution process is guaranteed to operate on a consistent stable state of the logic theory.

Finally, the resolution process depends on the \emph{general context} of the specific device hosting the LP service instance---thus considering the state of the KB therein available, as assembled by e.g., the set of sensors devices therein available, the service agents gathering new local information, and so on.

%---------------------------------
\paragraph{Stateless interactions.}
%---------------------------------
A first change brought by \lpaas{} concerns interaction with clients of the LP service.

In classical LP, interactions are necessarily \emph{stateful}: the user first sets the logic theory, then defines the goal, and then asks for one or more solutions, iteratively.
%
This implies that the LP engine is expected to store the logic theory to exploit as its KB, to memorise the goal under demonstration, and to track how many solutions have been already provided to the user---and all these items become part of the state of the LP engine.

Instead, in \lpaas{} interactions are first of all (even though not exclusively) \emph{stateless}: coherently with \soa{}, the LP service instance that actually serves each request may be different at each time, e.g.\ due to redundancy of distributed software components aimed at improving availability and reliability of the LP service.
%
In such a perspective, each client query (interaction) should be possibly self-contained, so that it does not matter which specific service instance responds---because there is no need for it to track the state of the interaction session.

It is worth emphasising that in the case of stateful interaction, adequate measures need be taken to prevent possible problems related to different \lpaas{} service instances serving repeated requests, requests from mobile clients, and similar situations.
%
Two possible solutions could be considered for this concern: 
\textit{(i)} the \lpaas{} middleware could simply \emph{lock} a service in case of stateful interaction, ensuring that the client always interacts with the same service instance (this is essentially the problem of \emph{conversational continuity}, well documented in the literature \cite{linda-toplas7}); 
\textit{(ii)} alternatively, the \lpaas{} middleware could take care of the \emph{hand-off} of the interaction from instance to instance, ensuring proper sharing of the information needed to preserve statefulness across service instances.

%---------------------------------
\paragraph{Time-local computation.}
%---------------------------------
Another change stemming from the situated nature of \lpaas{} is concerned with the relationship between the resolution process and the flow of time.

In pure LP, the logic theory is simply assumed to be always valid, and time-related aspects  do not affect the resolution process; for instance, assertion / retraction mechanisms are most typically regarded as extra-logic ones.
%
As discussed above, in \lpaas{} the consistency of the resolution process is guaranteed by the fact that the possibly ever-changing KB encapsulated by the service is \emph{frozen} in time when the resolution process itself begins: nevertheless, time situatedness requires by definition that time affects the LP service computation in some way.

Accordingly, in \lpaas{} each axiom in the KB is decorated with a \emph{time interval}, indicating the time validity of each clause.
%
Every time a new resolution process starts in order to serve a \lpaas{} request, the logic theory used is the one containing all and only the axioms holding true at the \emph{timestamp} associated with the resolution process itself.
%
In the simplest case, such a timestamp is implicitly assigned by the LP server as the current local time when the request for goal demonstration is first served.
%
Otherwise, it could also be explicitly assigned by clients along with the request---e.g., defining a specific time when asking for a goal demonstration.

%---------------------------------
\paragraph{Space-local computation.}
%---------------------------------
Analogously, classical LP has no notion of space situatedness: be it a virtual or a physical space, the LP engine is a monolithic component providing its ``services'' only locally, to its co-located ``clients'' working on the same machine.

The \lpaas{} interpretation stems again from the very nature of service in modern \soa{}-based applications---a computational unit providing its functionalities through a network-reachable endpoint.
%
Therefore, the resolution process in \lpaas{} is naturally and inherently affected by the specific \emph{computational locus} where a given LP service instance is executing at a given moment---there including the locally-available resources.


%===============================================================================
\section{The \lpaas{} Architecture}
\labelsec{architecture}
%===============================================================================

\lpaas{} is a logic-based, service-oriented approach for \emph{distributed situated intelligence}, conceived and designed as the natural evolution of LP in nowadays pervasive computing systems.
%
Its purpose is to enable situated reasoning via explicit definition of the spatio-temporal structure of the environment where situated entities act and interact.

Along the lines traced in \xss{towards-situated-service}, we now elaborate more practically on how encapsulation, statelessness, and locality -- that is, the \emph{service perspective} (\xss{service}) -- are exploited in \lpaas{} according to the three dimensions of \emph{situatedness} described in \xss{situatedness}---that is, time, space, and context.
%
Then, we briefly describe microservices  \cite{microservices-saasbook} as a key enabler architecture for \lpaas{}.


%------------------------------------------------------------------------------
\subsection{Service Architecture}
\labelssec{servicearchitecture}
%------------------------------------------------------------------------------

%---------------------------------
\paragraph{Encapsulation.}
%---------------------------------
As it straightforwardly stems from \soa{} principles, encapsulation is exploited in \lpaas{} so as to define a standard API that shields \lpaas{} clients from the inner details of the service while providing suitable means of interaction.

%%%%
\begin{figure}
	\includegraphics[width=0.475\linewidth]{images/advServiceArchitecture.png}
	\includegraphics[width=0.475\linewidth]{images/basicServiceArchitecture.png}
	\caption{\lpaas{} Configurator Service Architecture (left) and Client Service Architecture (right)}\labelfig{service}
\end{figure}
%%%%

Accordingly, each LP server node exposes its LP service to clients via two interfaces, depicted in \xf{service}:
%
\begin{description}
	\item[Client Interface] exposes methods for \emph{observation} and \emph{usage}.
		%
		\textit{Client} refers to any kind of users, either individuals (humans, software agents) or groups entitled to exploit the \lpaas{} services.
		%
	\item[Configurator Interface] enables service \emph{configuration} and requires proper access credentials.
		%
		\textit{Configurator} refers to service managers---privileged agents with the right of enforcing control and policies for that local portion of the system.
\end{description}
%
Applications can access the service as either \textit{Clients} or \textit{Configurators}, via the corresponding interfaces.
%
The service is initialised at deployment-time on the server machine: once started, it can be dynamically re-configured at run-time by any configurator.

%---------------------------------
\paragraph{Locality.}
%---------------------------------

Situatedness is exploited as a means to consistently handle \emph{locality} w.r.t.\ context, time, and space.

In fact, dealing with situated logic theories means to give up with the idea of global consistency in a closed world: in \lpaas{} multiple KB are spread throughout a netcitework infrastructure, likely geographically distributed, executing within different computational contexts, and possibly either fed by sensors or manipulated by service agents perceiving the physical context.
%
By allowing distributed access and reasoning over its own locally-situated knowledge base, each \lpaas{} node actively contributes to the overall availability of the global knowledge.

Accordingly, pervasive application scenarios where logic theories represent local knowledge inherently call for \emph{dynamic KB}, \emph{autonomously} evolving during the service lifetime\footnote{Here,``autonomously'' means that in the \lpaas{} perspective the logic KB may evolve over time with no need for a client to invoke \texttt{assert/retract}, or equivalent methods -- which, in fact, are not included in the \lpaas{} standard API detailed in \xss{api} -- but, e.g., due to sensor devices' perceptions transparently feeding the LP service KB.}.
%
As such, each situated KB of a \lpaas{} service can be seen as representing what is known to be true and relevant in a given location in space at a given time, thus possibly changing over time -- e.g., due to data streams coming from sensor devices --, and potentially different from any other KB located elsewhere---as depicted in \xf{sit-arch}.

%%%%
\begin{figure}
	\centering\includegraphics[width=0.60\linewidth]{images/LPaaSSituatedness.png}
	\caption{Situatedness of \lpaas{}: the same query ($q$) by the same client may be resolved differently ($\mathit{r}[s_{1}], \mathit{r}[s_{2'}], \mathit{r}[s_{2''}]$) by distinct \lpaas{} services ($\mathit{LPaaS}_{1}, \mathit{LPaaS}_{2'}, \mathit{LPaaS}_{2''}$) based on their local computational, physical, and spatio-temporal context ($\mathit{S}_{1}, \mathit{S}_{2'}, \mathit{S}_{2''}$)}
	\labelfig{sit-arch}
\end{figure}
%%%%

Accordingly,
%
\begin{itemize}
	\item each \lpaas{} clause has a lifetime, expressed as a time interval of validity---as in the case of the current temperature in a room;
	\item as a result, at any point $t$ in time a \lpaas{} service has precisely one logic theory made of all and only the clauses that hold true at time $t$;
	\item each \lpaas{} resolution process is either implicitly (by the \lpaas{} server) or explicitly (by the \lpaas{} client) labelled with a \emph{timestamp}, used to determine the KB to be used for the resolution itself---which then works as the standard LP resolution.
\end{itemize}

%---------------------------------
\paragraph{Statelessness.}
%---------------------------------
\emph{Uncoupling} is one of the main requirements for interaction in distributed systems: that is why \lpaas{} provides stateless client-server interaction as one of its main features.
%
The same holds true in particular for pervasive systems, where \emph{instability} is one of the main issues, as well as for mobile systems, with any sort of mobility: physical mobility of users and devices; users who change their computing device while using applications; service instances migrating from machine to machine---as in a cloud-based environment.

The need for uncoupling promotes \emph{stateless interaction} in \lpaas{}.
%
Thus, for instance, both \lpaas{} clients and service instances can freely move with no concerns for requests tracking and identity/location bookeeping.

In order to balance the effect of statelessness on data-intensive interactions between LP service and users, \lpaas{} also provides clients with the ability to ask for more than one solution at a time, and even all of them, with a single request.
%
Nevertheless, \lpaas{} also makes it possible to obtain a \emph{stream of solutions} from the resolution process, rather than a single solution at a time in an individual interaction session, to better meet the needs of fast-paced dynamic scenarios in which clients want to be constantly updated by the LP service about some situation.

Accordingly, \lpaas{} provides clients with the means to obtain both stateless and stateful client-server interaction:
%
\begin{description}
	\item[stateful] once the logic theory to consider is settled, and the goal stated, the client should be able to ask for any amount of solutions -- possibly iteratively, possibly at different times and from different places -- with the service being responsible to guarantee consistency and validity of solutions by keepinimagesg track of the related interaction sessions with the same client;
	\item[stateless] in this case, no session state is tracked by the server, so each client request should contain all the information required to serve the request itself.
\end{description}
%
It is worth highlighting that nothing prevents the service from being stateful and stateless simultaneously, because the LP server can manage multiple kinds of requests concurrently; instead, of course, each client request in \lpaas{} is either stateful or stateless.

%------------------------------------------------------------------------------
\subsection{{M}icroservices as Technology Enablers}
\labelssec{microservices}
%------------------------------------------------------------------------------
Service-oriented architectures represent nowadays the standard approach for distributed system engineering \cite{soabook-2005}: so, \lpaas{} adopts the \emph{Software as a Service} (\saas{}) architecture as its reference \cite{saas-cacm53}.

Accordingly, information technology resources are conceived as continuously-provided services:
\saas{} applications are supposed to be available 24/7, scale up \& down elastically, support resiliency to changes (i.e., in the form of suitable fault-tolerance mechanisms), provide a responsive user experience on all popular devices, and require neither user installation nor application updates.

In particular, LP services in \lpaas{} can be fruitfully interpreted as \emph{microservices} \cite{microservices-saasbook}.
%
Microservices are a recent architectural style for \saas{} applications promoting usage of self-contained units of functionally with loosely-coupled dependencies on other services: as such, they can be designed, developed, tested, and released independently.
%
Thanks to their features, microservices are deserving increasing attention also in the industry -- pretty much like \soa{} in the mid 2000s --  where fast and easy deployment, fine-grained scalability, modularity, and overall agility are particularly appreciated \cite{microservicespitfalls-book2016}.

Technically speaking, microservices are designed to expose their functionality through standardised network-addressable APIs and data contracts, making it possible to choose the programming language, operating system, and data store that best fit the service needs and the developers' skills set, without worrying about interoperability.
%
Microservices should also be dynamically \emph{configurable}, possibly in different forms and with different configuration levels.
%
Obviously, actual support to interoperability requires multiple levels of standardisation: to this end, \lpaas{} defines its own \emph{interfaces} for both \emph{configuration} and \emph{exploitation}, while relying on widely adopted standards as far as the representation formats (i.e., \cite{JSON-home}) and interaction protocols (i.e.\ REST over HTTP, or \cite{MQTT-home}) are concerned.




