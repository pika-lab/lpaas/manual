%=======================================================================
\chapter{\lpaas{} Basics}
\label{ch:engine}
%=======================================================================

This chapter overviews the basic elements and structure of the \lpaas{} engine, the \tuprolog{} syntax, the programming support, and the built-in predicates.
%

%-------------------------------------------------------------------------------
\section{\lpaas{} API}\labelsec{lpaas:api:logical}
%%-------------------------------------------------------------------------------
The \lpaas{} API methods are detailed in \xt{LPaaS-ConfigInterface} (Configurator Interface and Source Interface), and \xt{LPaaS-Interface} (Client Interface)---adopting the standard \prolog{} notation for input/output where \texttt{``+"}  means that the argument must be instantiated (input argument), \texttt{``-"} that it is an output argument (usually non-instantiated, unless one wants to check for a specific return value), and \texttt{``?"} that it can be either instantiated or not \cite{prologstandard-book1996}.

As far as the logic theory is concerned, a \emph{static} KB is immutable, while a \emph{dynamic} KB can evolve during the server lifetime, thus implying that clauses have a lifetime too and can be asserted and retracted as needed ---for instance, a clause representing the current temperature in a room.
%
The static knowledge base reflects the view of LP based on a static representation of knowledge, with the standard LP semantics, under the closed-world assumption (CWA) \cite{knowledge-representation-systems1992Beierle}: in \iot{} scenarios, this possibility is of great interest when dealing with invariants, such as properties or statements that must be always guaranteed to be true, no matter what. 
In our model the dynamic knowledge base, which takes the time dimension into account, constitutes a more general case than the static one: accordingly, the static KB predicates can be seen as a simpler subset of the dynamic KB ones, discussed below.


Client interaction can be either \emph{stateless} or \emph{stateful}: in the former case, each request is self-contained -- thus, always repeats the goal to be proven -- while in the latter case requests are tracked on the server side, so that once the goal is set there is no need to repeat its specification in subsequent requests.
%
It is worth noting that while the service can act simultaneously statefully and statelessly -- as it can manage different kinds of request concurrently -- the knowledge base is by its nature either dynamic or static.

%%%%%%%%%%%%%%%%%
\begin{table}[!b]
\caption{\lpaas{} Configurator Interface (left) and \lpaas{} Source Interface (right)}\labeltab{LPaaS-ConfigInterface}
\scriptsize
\noindent \begin{minipage}[t]{0.43\linewidth}
%\begin{table}[]
%\caption{\lpaas{} Configurator Interface}\labeltab{LPaaS-ConfigInterface}
%\centering
%\flushleft
\resizebox{\linewidth}{!}{%
\begin{tabular}{c}
\hline\hline
\textbf{Configurator Interface}		\\
\hline\hline
\tt setConfiguration(+ConfigurationList)\\
\tt getConfiguration(-ConfigurationList)\\
\tt resetConfiguration()\\
\\
\tt setTheory(+Theory)\\
\tt getTheory(-Theory)\\
\tt setGoals(+GoalList)\\
\tt getGoals(-GoalList)\\
\hline\hline
\end{tabular}
}
%\end{table}
\end{minipage}
\begin{minipage}[t]{0.28\linewidth}
%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%
%\begin{table}[]
%\caption{\lpaas{} Source Interface}\labeltab{LPaaS-SourceInterface}
\centering
%\resizebox{\linewidth}{!}{%
\begin{tabular}{c}
\hline\hline
\textbf{Source Interface}		\\
\hline\hline
\tt addFacts(+Facts)\\
\tt getFacts(-Facts, template(?Template), ?Timestamp) \\
\\
\\
\\
\\
\\
\hline\hline
\end{tabular}
%}
%\end{table}
\end{minipage}
\end{table}
\normalsize

The LP service offers a set of methods to provide configuration and contextual information about the service and its KB, and a set of methods to query the service for triggering computations and reasoning, and to ask for solutions.
In particular, the \emph{Configurator Interface} makes it possible to set the service configuration, its KB, and the list of admissible goals.
%
The \emph{Source Interface} provides for adding new facts to the KB, meant to represent sensors and actuators with latest measurements or actions feedback.
%
The \emph{Client Interface} makes it possible querying the service about its configuration parameters (stateful/stateless and static/dynamic), the state of the KB, the admissible goals and facts. 
Moreover, the interface allows the service to be asked for one solution, N solutions, or all solutions available.
%
These operations are slightly different in case of stateless or stateful requests: in the first case, the \texttt{solve} operation is conceptually atomic and self-contained -- always has \texttt{Goal} as its parameter -- whereas in the latter case such a self-containment is not necessary, since the server keeps track of the client state: so, the goal can be set only once before the first \texttt{solve} request is issued.

%primitive situate nel tempo
Unlike classical LP, \lpaas{} also provides an extra \texttt{solve} operation for dealing with \emph{streams of data} (i.e.\ stream of solutions), that are often found in \iot{} contexts.
%
This extra operation is particularly useful when accounting for Source Clients: since they are meant to add data to the \lpaas{} KB dynamically -- and \emph{monotonically} -- it makes sense to allow Clients to get a novel solution to the goal set each $t$ time steps (parameter \texttt{every(@Time)}), to promptly react to changes in contextual data---such as the temperature of a room.
%
In addition, a family of \texttt{solve} predicates with a specific \texttt{within(+\textit{Time})} argument (intended as server-side local time) prevents the server from being indefinitely busy: if the resolution process does not complete within the given time, the request is cancelled, and a failure response is returned to the client.

Besides these novelties, mostly regarding efficient interaction among the service and its clients, time can affect computations, leading to \emph{time-dependant computations}, and the \emph{temporal validity} of logic theories as well, thus of all the individual facts and clauses therein, whose validity can be collectively bound to a given time horizon---i.e., being true up to a certain instant in time, and no longer since then.
Computation requests may then arbitrarily restrict the \emph{temporal scope} of the expected solutions, specifying the temporal bounds of the logical facts and clauses to consider as valid while proving a goal.
%
This is the way in which \lpaas{} supports temporal situatedness, that is, the ability to be aware of time passing by and of the fact that, in dynamic scenarios like the \iot{}, the knowledge base represents \emph{truth at a given point in time}.

%primitive situate nello spazio
With respect to space situatedness (\xss{lpaas:situatedness}), the \lpaas{} server inherently has a notion of its own location, since the service container is physically located somewhere: by specifying the ``width'' of a region, defined according to some (custom) metric, we can define the service \emph{surroundings}. The key point is that \lpaas{} can explore its surroundings to discover other \lpaas{} instances, representing different local knowledge, and forward to them the query looking for expanding the solutions it found by itself.
%
This concept is captured by the \texttt{solveNeighborhood/2} primitive that associates the inference process to a region centred in \texttt{\textit{Pos}} -- if omitted, the server position is considered -- and distance specified by \texttt{\textit{Distance}}.
%
Similarly to the time case, the client could also open a \emph{stream of solutions across space}: this could be interpreted, for instance, as widening/narrowing the region to be considered at each cycle of the resolution process. The extra \texttt{\textit{Span}} argument in the same primitive satisfies this requirement, specifying how much to widen/narrow the range at each cycle.

Finally, the \texttt{reset} primitive clears the resolution process, effectively restarting resolution, with no need to reconfigure the service (i.e., select the goal); in turn, the \texttt{close} primitive actually ends communication with the server---so that the goal must be re-set in order to restart querying the server.

%%%%%%%%%%%%%%%%%%%%%
%%%
\begin{landscape}
\footnotesize
\begin{table}
\caption{\lpaas{} Client Interface.}
\labeltab{LPaaS-Interface}
%\begin{minipage}{\textwidth}\scriptsize
\resizebox{\linewidth}{!}{%
\begin{tabular}{cc}
\hline\hline
\multicolumn{2}{c}{\textbf{DYNAMIC KNOWLEDGE BASE}}		\\
\hline\hline
\textbf{Stateless}	& \textbf{Stateful}\\
\hline
\multicolumn{2}{c}{\tt{getServiceConfiguration(-ConfigList)}}		\\
\multicolumn{2}{c}{\tt getTheory(-Theory, ?Timestamp)}		\\
\multicolumn{2}{c}{\tt getGoals(-GoalList)} 					\\
\multicolumn{2}{c}{\tt isGoal(+Goal)} 						\\

& \tt setGoal(template(+Template)) 	\\
& \tt setGoal(index(+Index)) 	\\
 \\
\tt solve(+Goal, -Solution, ?Timestamp)       	& \tt solve(-Solution, ?Timestamp) 	\\
\tt solveN(+Goal, +NSol, -SList, ?TimeStamp)       		& \tt solveN(+NSol, -SolutionList, ?TimeStamp) \\
\tt solveAll(+Goal, -SList, ?TimeStamp)		       		& \tt solveAll(-SolutionList, ?TimeStamp) 	 \\
\\
\tt solve(+Goal, -Solution, within(+Time), ?TimeStamp)       	& \tt solve(-Solution, within(+Time), ?TimeStamp) 	\\
\tt solveN(+Goal, +NSol, -SList, within(+Time), ?TimeStamp) & \tt solveN(+NSol, -SList, within(+Time), ?TimeStamp) \\
\tt solveAll(+Goal, -SList, within(+Time), ?TimeStamp)	& \tt solveAll(-SList, within(+Time), ?TimeStamp)	\\
\\
\tt solveAfter(+Goal, +AfterN, -Solution, ?TimeStamp)		& 				 \\
\tt solveNAfter(+Goal, +AfterN, +NSol, -SList, ?TimeStamp)		&  			 \\
\tt solveAllAfter(+Goal, +AfterN, -SList, ?TimeStamp)	& 			 \\

& \tt solve(-Solution, every(+Time), ?TimeStamp)	\\
& \tt solveN(+N, -SList, every(+Time), ?TimeStamp)	\\
& \tt solveAll(-SList, every(+Time), ?TimeStamp)	\\
& \tt pause()	\\
& \tt resume()	\\
\\
\tt solveNeighborhood(+Goal, -Solution, region(?P, +Space), ?TimeStamp) &\tt solveNeighborhood(-Solution, region(?P, +Space), ?TimeStamp)\\
\tt solveNNeighborhood(+Goal, +NSol, -SList, region(?P, +Space), ?TimeStamp) &\tt solveNNeighborhood(+NSol, -SList, region(?P, +Space), ?TimeStamp)\\
\tt solveAllNeighborhood(+Goal, -SList, region(?P, +Space), ?TimeStamp) &\tt solveAllNeighborhood(-SList, region(?P, +Space), ?TimeStamp)\\
\\
 &\tt solveNeighborhood(-SList, region(?P, +Space, +Span), ?TimeStamp)\\
 &\tt solveNNeighborhood(-SList, region(?P, +Space, +Span), ?TimeStamp)\\
 &\tt solveAllNeighborhood(-SList, region(?P, +Space, +Span), ?TimeStamp)\\
% & \tt pause()	\\
% & \tt resume()	\\

\tt solveNeigh.After(+Goal, +AfterN, -SList, region(?P,+Space,+Span), ?TimeS) &\\
\tt solveNNeigh.After(+Goal, +AfterN, +N, -SList, region(?P,+Space,+Span), ?TimeS) &\\
\tt solveAllNeigh.After(+Goal, +AfterN, -SList, region(?P, +Space, +Span), ?TimeS) &\\
\\
\multicolumn{2}{c}{\tt reset()} 					\\
\multicolumn{2}{c}{\tt close()} 						\\
\hline\hline
\end{tabular}
}
\\\\
\centering\scriptsize{The table reports only methods operating on a dynamic KB that take an additional \texttt{Timestamp} argument, expressing the required time validity; static KB methods are analogous without the \texttt{Timestamp} parameter: for a full description of the API, we refer the reader to \cite{lpaas-ijguc2018}.}
\end{table}
\end{landscape}
\normalsize
%%%%



%===============================================================================
\section{RESTful \lpaas{}: Development Process from Design to Deployment}\labelsec{lpaas-rest}
%===============================================================================

As a first prototype, we chose to realise \lpaas{} as a Web Service in compliance with the \emph{Respresentational State Transfer} (REST) architectural style \cite{Fielding2000}, thus re-interpreting \lpaas{} main features and its API (\xt{LPaaS-ConfigInterface} and \xt{LPaaS-Interface}) in terms of \emph{web resources} accessible through a stateless HTTP-based interface.
%
This allows both clients and \lpaas{} itself to be implemented with different technologies and programming languages, while remaining mutually interoperable.

Of course, in a RESTful implementation, the statefulness disappears so as to better comply to REST principles.
%
However, it is worth emphasising here that this is just one among the possible implementations of \lpaas{}, thus, since other design choices could benefit from a stateful approach, the corresponding generic LP API should not be disregarded.
%
For instance, by adopting the SOA architectural style, we could genuinely take statefulness into account, while retaining the advantages of leveraging a web-service implementation.

The following subsections discuss the \restful{} API for \lpaas{}, the corresponding implementation, and its deployment process. 

%-------------------------------------------------------------------------------
\subsection{\restful{} API Design}\labelssec{rest}
%-------------------------------------------------------------------------------
%%%%
\begin{figure}[t]
	\centering
  	%\hfill
  	\includegraphics[scale=.4]{images/RESTapi1.png}
  	%\hfill
  	\includegraphics[scale=.47]{images/RESTapi2.png}
  	%\hfill
  	\caption{The \lpaas{} \restful{} API. For further details please refer to \lpaas{} Home Page \cite{lpaas-home}. }
  	\labelfig{RESTapi}
\end{figure}
%%%%

%

The three interfaces from \xss{service} are collapsed within a unified \restful{} API exposingimages the same \emph{resources} to clients possibly playing different \emph{roles}---namely \emph{client}, \emph{source}, or \emph{configurator}.
%
The two latter roles, since they are capable of applying \emph{side-effects} to the service KB, require authentication.
%
There are four main REST resource types provided: \texttt{theories}, \texttt{goals}, \texttt{solutions}, and \texttt{auth}orisation tokens---as shown in \xf{RESTapi}, along with their sub-resources and supported operations.

All resources are accessible by specifying the so-called \emph{entry point} after the service base URL: for instance, \texttt{www.lpaas-example.it\textit{/theories}}: clients may exploit the logic service functionalities by issuing HTTP requests on the corresponding entry point.
%
The response contains the requested information (if possible), an acknowledgement, or some link for late access to the result.
%
For instance, users wanting to play some privileged role \texttt{POST} their credential to the \texttt{/auth} entry point.
%
A comprehensive description of the \restful{} entry points and their supported HTTP methods is provided in \xt{LPaaS-Rest-Api}, in terms of mapping the \lpaas{} interface API given in \xt{LPaaS-ConfigInterface} and \xt{LPaaS-Interface}.

\emph{Configurators} can create theories by \texttt{POST}ing a well-formed Prolog theory on the \texttt{/theories} entry point.
%
Multiple \emph{named} theories and, for each theory, many versions can be maintained. 
%
According to time situatedness, each version of a theory is valid since the instant it has been \texttt{POST}ed (service local time) and until either a newer version is \texttt{POST}ed, or some modification is made through some other entry point. 
%
The specific version, say the one valid at time \texttt{\textit{t}}, of a particular theory \texttt{\textit{n}} can be read by clients via a \texttt{GET} operation at the \texttt{/theories/\textit{n}/history/\textit{t}} entry point.
%
As concerns spatial situatedness, whenever an information -- i.e., theory or fact version -- is retrieved via a \texttt{GET} operation, the response includes a spatial tag defining the position in space where the returned information can be considered valid.

Theory versions make it possible to preserve LP consistency despite the side effects possibly occuring on dynamic KBs: in this way, clients are guaranteed to re-obtain the same solution
if the same goal(s) is re-asked on the same theory version.

%---
\paragraph{\emph{Configurator.}}
%---
Other than having the right to create or edit theories, clients playing the \emph{configurator} role can create individual goals or lists of goals (\emph{composed-goals}) by \texttt{POST}ing a well-formed conjunction of Prolog terms on the \texttt{/goals} entry point, along with a mnemonic name (say \texttt{\textit{g}}).
%
Goals can be lately appended to the list by \texttt{POST}ing them on the \texttt{/goals/\textit{g}} entry point, while the \texttt{\textit{i}}-th subgoal of \texttt{\textit{g}} can be replaced by \texttt{PUT}ing \texttt{\textit{g'}} on the \texttt{/goals/\textit{g}/\textit{i}} entry point.
%
Of course, configurators are also authorised to perform any other operation, there including the ones available to source or client roles.

%---
\paragraph{\emph{Source.}}
%---
Sensors, actuators, or other clients playing the \emph{source} role may also inject (or update) novel information -- in the form of logic \emph{facts} wrapped by a proper functor (say \texttt{\textit{f}}) -- in a theory \texttt{\textit{n}} by \texttt{POST}ing (resp.\ \texttt{PUT}ting) the new information to the \texttt{/theories/\textit{n}/facts/\textit{f}} entry point.
%
The latest version of a specific fact \texttt{\textit{f}} in theory \texttt{\textit{n}} can be read by any client \texttt{GET}ting the aforementioned entry point, while the fact version valid at \texttt{\textit{t}} can be obtained querying the entry point \texttt{/theories/\textit{n}/facts/\textit{f}/history/\textit{t}}.


%---
\paragraph{\emph{Client.}}
%---
Any client can get the list of admissible goals by \texttt{GET}ting the \texttt{/goals/\textit{g}} entry point, and inspect the \texttt{\textit{i}}-th sub-goal of \texttt{/goals/\textit{g}} by \texttt{GET}ting the \texttt{/goals/\textit{g}/\textit{i}} entry point.

Given the references (i.e., the URLs) to a theory version and a goal-list, any client may request one or more solutions for the composed-goal by \texttt{POST}ing such references to the \texttt{/solutions} entry point. 
%
The request may contain a number of \emph{optional} parameters that correspond to the parameters shown in \xt{LPaaS-ConfigInterface} and \xt{LPaaS-Interface} ---namely:
\begin{itemize}
	\item \texttt{skip} defining the amount of solutions to be skipped (defaults to 0)
	\item \texttt{limit} defining the maximum amount of solutions to be retrieved (defaults to $\infty$)
	\item \texttt{within} defining the maximum amount of time the server may spend searching for a solution (defaults to $\infty$)
	\item \texttt{every} defining the amount of time between two consecutive solutions, meaning that a periodic solution stream is actually desired %(defaults to ``do not periodically re-execute the resolution'')
	\item \texttt{hook} defining the \emph{hook} associated to the request, that is, a mnemonic name used to retrieve \emph{ex-post} a solution previously computed by the \lpaas{} server% (defaults to an automatically generated UUID)
\end{itemize}
%
The following optional parameters support situatedness in space:
%
\begin{itemize}
	\item \texttt{where} defining the centre of the region of validity to be considered for the solution, meaning that the resolution process should only take into account the logic theories and facts being valid within that  when the resolution process starts % (defaults to the current position of the logic service's hosting machine)
	\item \texttt{regionSize} defining the \emph{size} of the aforementioned validity region %(defaults to 0, meaning that only the service position should be taken into account)
	\item \texttt{regionSpan} defining the \emph{variation in size} to add to the \texttt{regionSize} value at each step of the resolution process
\end{itemize}
%
The \texttt{every} and \texttt{regionSpan} parameters are aimed at supporting \emph{streams of solutions} in addition to a single set of solutions: the first initiates a time-related stream of solutions where the provided query is resolved periodically, while the second initiates a space-related stream of solutions where the provided query is resolved against a progressively widening/narrowing validity region (depending on the sign of \texttt{regionSpan}).

The aforementioned hook makes it possible to retrieve already computed solutions by \texttt{GET}ting the resource located at \texttt{/solutions/{hook}}, thus providing a sort of cache memory.
%
Its value, say \texttt{\textit{h}}, is of particular interest in case a client needs to consume the results of a streaming request, since it refers to the very last outcome of the resolution process: users may then issue a \texttt{GET} request towards the \texttt{/solutions/\textit{h}} entry point to retrieve the latter available result of solution stream, while previous solutions are accessible through an \emph{ad-hoc} entry point. %\rcrev{Giovanni qui sarebbe meglio dire quale visto che prima li diciamo}. 

\begin{table}
\caption{The \restful{} reification of \lpaas{} API.
	}
	\labeltab{LPaaS-Rest-Api}
	\centering
	\scriptsize
	%\begin{tabular}{ll|c}
	\begin{tabular}{ll|c}
			
		\hline\hline
		\multicolumn{3}{c}{\textbf{Legend}}	\\
		\hline\hline
		
		\myEntry{\httpMethod{\textbf{HTTP\_METHOD:}}{/resource}\httpQueryA{queryParam}{value}}{Request content}{Response Content}{\textbf{\lpaas{} Interface API}}
		
		\hline\hline
		\multicolumn{3}{c}{\textbf{\emph{configurator} role}}	\\
		\hline\hline
		
		\myHttpPlEntry{\httpPost{/theories/\var{default}}}{\Var{T}}{-}{setTheory(\Var{T})}
		
		\hline
		
		\myHttpPlEntry{\httpPut{/goals/\var{default}}}{\Var{G}}{-}{setGoals(\Var{G})}
		
		\hline
		
		\hline\hline
		\multicolumn{3}{c}{\textbf{\emph{source} role}}	\\
		\hline\hline
		
		\hline
		
		\myHttpPlEntry{\httpGet{/theories/\var{default}/facts/\Var{f}}}{-}{\Var{F}}{getFacts(\Var{F}, template(\Var{f}(\_)), \_)}
		
		\hline
		
		\myHttpPlEntry{\httpGet{/theories/\var{default}/facts/f/history/\Var{V}}}{-}{\var{F}}{getFacts(\var{F}, template(f(\_)), \Var{V})}
		
		\hline
		
		\myHttpPlEntry{\httpPost{/theories/\var{default}/facts}}{\Var{F}}{-}{addFacts(\Var{F})}
		
		\hline\hline
		\multicolumn{3}{c}{\textbf{\emph{client} role}} \\
		\hline\hline
		
		\myHttpPlEntry{\httpGet{/theories/\var{default}}}{-}{\Var{T}}{getTheory(\Var{T}, \_)}
		
		\hline
		
		
		\myHttpPlEntry{\httpGet{/theories/\var{default}/history/\Var{V}}}{-}{\var{T}}{getTheory(\var{T}, \Var{V})}
		
		\hline
		
		
		\myHttpPlEntry{\httpGet{/goals/\var{default}}}{-}{\Var{G}}{getGoals(\Var{G})}
		
		\hline
		
		\myHttpPlEntry{\httpPost{/solutions}\httpQueryA{limit}{1}}{\Var{G}}{\Var{S}}{solve(\Var{G}, \Var{S}, \_)}
		
		\hline
		
		\myHttpPlEntry{\httpPost{/solutions}\httpQueryA{limit}{1}}{\var{G}, \Var{V}}{\var{S}}{solve(\var{G}, \var{S}, \Var{V})}
		
		\hline
		
		\myHttpPlEntry{\httpPost{/solutions}\httpQueryA{limit}{\Var{N}}}{\var{G}, \var{V}}{\var{S}}{solve{N}(\var{G}, \Var{N}, \var{S}, \var{V})}
		
		\hline
		
		\myHttpPlEntry{\httpPost{/solutions}}{\var{G}, \var{V}}{\var{S}}{solve{All}(\var{G}, \var{S}, \var{V})}
		
		\hline
		
		\myHttpPlEntry{\httpPost{/solutions}\httpQueryAA{limit}{1}{within}{\Var{T}}}{\var{G}, \var{V}}{\var{S}}{solve(\var{G}, \var{S}, within(\Var{T}), \var{V})}
		
		\hline
		
		\myHttpPlEntry{\httpPost{/solutions}\httpQueryAA{limit}{\Var{N}}{within}{\var{T}}}{\var{G}, \var{V}}{\var{S}}{solve{N}(\var{G}, \Var{N}, \var{S}, within(\var{T}), \var{V})}
		
		\hline
		
		\myHttpPlEntry{\httpPost{/solutions}\httpQueryA{within}{\var{T}}}{\var{G}, \var{V}}{\var{S}}{solve{All}(\var{G}, \var{S}, within(\var{T}), \var{V})}
			
		\hline
		
		\myHttpPlEntry{\httpPost{/solutions}\httpQueryAA{skip}{\Var{N}}{limit}{1}}{\var{G}, \var{V}}{\var{S}}{solve{After}(\var{G}, \Var{N}, \var{S}, \var{V})}
				
		\hline
		
		\myHttpPlEntry{\httpPost{/solutions}\httpQueryAA{skip}{\Var{N1}}{limit}{\Var{N2}}}{\var{G}, \var{V}}{\var{S}}{solveNAfter(\var{G}, \Var{N1}, \Var{N2}, \var{V})}
		
		\hline
	
		\myHttpPlEntry{\httpPost{/solutions}\httpQueryA{skip}{\var{N}}}{\var{G}, \var{V}}{\var{S}}{solve{All}After(\var{G}, \var{N}, \var{S}, \var{V})}
		
		\hline
		
		\multicolumn{2}{l|}{\httpPost{/solutions}\httpQueryAA{every}{\Var{DT}}{hook}{\Var{H}}}&\multirow{4}{*}{\shortstack{\texttt{setGoal(template(\var{G})),}\\\texttt{solve(\var{S}, every(\Var{DT}), \var{V})}}}\\
		\httpReq{\var{G}, \var{V}}&\httpRes{\Var{H}}&\\
		\multicolumn{2}{l|}{\httpGet{/solutions/\Var{H}}}&\\
		\httpReq{-}&\httpRes{\var{S}}&\\
		
		\hline
		
		\myHttpPlEntry{\httpPost{/solutions}\httpQueryAA{where}{\Var{W}}{regionSize}{\Var{R}}}{\var{G}, \var{V}}{\var{S}}{solve{Neighborhood}(\var{G}, \var{S}, region(\Var{W}, \Var{R}), \var{V})}
		
		\hline
		
		\multicolumn{2}{l|}{\shortstack{\httpPost{/solutions}\httpQueryAAAA{where}{\var{W}}{regionSize}{\var{R}}{regionSpan}{\Var{DS}}{hook}{\Var{H}}}}&\multirow{4}{*}{\shortstack{\texttt{setGoal(template(\var{G})),}\\\texttt{solveNeighborhood(\var{G}, \var{S},}\\\texttt{\hfill{}region(\var{W}, \var{R}, \Var{DS}), \var{V})}}}\\
		\httpReq{\var{G}, \var{V}}&\httpRes{\Var{H}}&\\
		\multicolumn{2}{l|}{\httpGet{/solutions/\Var{H}}}&\\
		\httpReq{-}&\httpRes{\var{S}}&\\
		
		
		\hline\hline
		
	\end{tabular}
\end{table}


%-------------------------------------------------------------------------------
\subsection{The Development Process}
%-------------------------------------------------------------------------------

Delivering an LP engine (i.e. a logic-based inference service) as a microservice in the \iot{} landscape means not only to ensure non-functional properties such as modularity, loose coupling, and scalability, but also to affect the software engineering practice, thus the development process, in itself.
%
Continuous Integration (CI) \cite{continuousDelivery-humble2010} and, most importantly, Continuous Delivery (CD) \cite{continuousIntegration-duvall2007} pipelines are a de-facto standard for developing microservices, since they comply with agile methodologies and effectively promote the best practices concerning software architecture and implementation.

\xf{DevProc} shows the scheme of the \lpaas{} development process: the service implementation supports container-based deployment via the Docker technology, further discussed in \xss{docker}-- and the development process supports CI and CD, so as to provide fast updates and improvements to devices hosting the \lpaas{} service.

In particular, the distributed software development \cite{lpaas-home} relies on the GitLab source code repository and Git\footnote{\url{https://git-scm.com/}} as a versioning tool. 
%
Build cycles are handled through the Gradle build system\footnote{\url{https://gradle.org/}} (migrating from Maven), while regression/integration testing amongst inner microservices exploits GitLab's CI/CD Pipelines feature\footnote{\url{https://docs.gitlab.com/ee/ci/pipelines.html}}. 
%
Finally, the deployment on the Docker platform\footnote{\url{https://www.docker.com/what-docker}} relies on integration with the GitLab pipeline through the Docker Engine feature\footnote{\url{https://docs.gitlab.com/ce/ci/docker/using_docker_build.html}}.

%%%%
\begin{figure}
	\centering
  	\includegraphics[width=\linewidth]{images/process.png}
  	\caption{The \lpaas{} Development Process}\labelfig{DevProc}
\end{figure}
%%%%


%-------------------------------------------------------------------------------
\subsection{\restful{} API Implementation}
%-------------------------------------------------------------------------------
In order to  adhere to the \restful{} API description, the KB of the service is fully versioned: so any side-effects caused by the Configurator or by Sources either directly, through POST/PUT methods, or indirectly, via \texttt{assert} / \texttt{retract} predicate, on some theory automatically produces a new version of that theory.
%
Versioned theories are referenceable by means of specific URLs and are made persistent through the storage layer: thanks to the uncoupling provided by the PA\footnote{\url{http://www.oracle.com/technetwork/java/javaee/tech/persistence-jsp-140049.html}}
object relational the specific target technology is irrelevant.
%
Client request always specify (the URL of) the desired version of the theory: 
%
If no version is provided, the last available one is assumed.
%
If no theory is provided at all, the \texttt{/theories/default} one is assumed.
%
Analogously, for both goals and solutions entry points, if no list of goals is provided, the \texttt{/goals/default} is assumed.
If no \emph{hook} is specified, a novel and unique one is generated on the fly and provided to the client together with the solution.

\emph{Stream} solutions -- which require a resolution process to be re-executed periodically -- are made resilient by making the request persistent on the storage layer: this implies that the solution stream continues to be produced even if the \lpaas{} server crashes and has to be restarted.

As far as security is concerned, authentication is achieved via JSON Web Tokens (JWT) technology.
%
The \lpaas{} service provides -- in case credentials are valid -- a cryptographically signed token within the HTTP response, certifying the client's role(s).
%
It is the client's responsibility, in order to be recognised as a Configurator or a Source, to provide the signed token by means of the \texttt{Authorization} HTTP header of any following request.


%-------------------------------------------------------------------------------
\subsection{Deployment by Containerization}\labelssec{docker}
%-------------------------------------------------------------------------------

Containerisation is a means for developing, testing, and deploying virtually any kind of application on virtually any kind of infrastructure, either centralised or distributed.
%
Containers consist of lightweight, portable, and scalable virtual environments wrapping a particular application -- \lpaas{} microservices in our case -- and only including the \emph{runtime} libraries it strictly needs.
%
Containers can be serialised, copied on different machines and there re-executed or arbitrarily replicated, regardless of the specific configuration of the hosting machine.
%
Docker \footnote{\url{https://www.docker.com}} is arguably the most interoperable technology supporting containerisation on main-stream operating systems such as Windows, Linux, or MacOS.
%
Deploying an application with Docker requires to build an \emph{image} of the application, that is, an executable package featuring everything needed to run the application---the code, a runtime, libraries, environment variables, and configuration files.

In our case, a Dockerfile is created for the \lpaas{} image using the Docker runtime.
%
Then, a containerised application can be deployed either as a standalone \emph{service}, or as a \emph{microservice} thet is part of a \emph{stack} of services depending on one another.

The business logic component of the \lpaas{} architecture is executable as a standalone application, as well as the Data Layer component, which at present may be based on either an embedded DB provided by the Payara Micro edition\footnote{\url{https://www.payara.fish/payara_micro}} or an external DB such as PostgreSQL\footnote{\url{https://www.postgresql.org/}} thanks to JPA.
%
Actually, the \lpaas{} engine and the KB are independent microservices now, deployed on a common \emph{network}, isolated from any other stack of services.
%
Such a stack can then be run either on a single machine or on a \emph{swarm} of containers/hosts through the \emph{Docker Swarm} functionality, thus going fully distributed.
%
The \lpaas{} deployment described in this paper follows the latter approach, taking advantage of all the aforementioned functionalities provided by the Docker technology.

